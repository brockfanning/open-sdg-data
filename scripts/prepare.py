import os
import sdg

# Create the input object.
# Input data from CSV files matching this pattern: data/*-*.csv
data_pattern = os.path.join('data', '*-*.csv')
data_input = sdg.inputs.InputCsvData(path_pattern=data_pattern)

# Input metadata from YAML files matching this pattern: meta/*-*.md
meta_pattern = os.path.join('meta', '*-*.md')
meta_input = sdg.inputs.InputYamlMdMeta(path_pattern=meta_pattern)

# Combine these inputs into one list.
inputs = [data_input, meta_input]

# Use the Prose.io file for the metadata schema.
schema_path = '_prose.yml'
schema = sdg.schemas.SchemaInputOpenSdg(schema_path=schema_path)

# Create an "output" from these inputs and schema, for JSON for Open SDG.
output = sdg.outputs.OutputOpenSdg(inputs, schema, output_folder='public')
