.PHONY: install test build
all: install test build

install:
	pip3 install -r scripts/requirements.txt

test:
	python3 scripts/test.py

build:
	python3 scripts/build.py